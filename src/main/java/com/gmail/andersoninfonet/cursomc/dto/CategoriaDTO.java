package com.gmail.andersoninfonet.cursomc.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.gmail.andersoninfonet.cursomc.model.Categoria;

public class CategoriaDTO implements Serializable {

	private static final long serialVersionUID = 2931682676557060033L;
	
	private Long id;
	
	@Length(min = 5, max = 80, message = "O tamanho deve ser entre 5 e 80 caratecres.")
	@NotEmpty(message = "Preenchimento obrigatorio.")
	private String nome;
	
	public CategoriaDTO() {
	
	}
	
	public CategoriaDTO(Categoria categoria) {
		this.id = categoria.getId();
		this.nome = categoria.getNome();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
