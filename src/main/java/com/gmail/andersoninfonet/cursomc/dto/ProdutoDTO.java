package com.gmail.andersoninfonet.cursomc.dto;

import java.io.Serializable;

import com.gmail.andersoninfonet.cursomc.model.Produto;

public class ProdutoDTO implements Serializable {

	private static final long serialVersionUID = -1700565418218294317L;
	
	private Long id;
	private String nome;
	private Double preco;
	
	public ProdutoDTO() {
		
	}
	
	public ProdutoDTO(Produto p) {
		id = p.getId();
		nome = p.getNome();
		preco = p.getPreco();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
}
