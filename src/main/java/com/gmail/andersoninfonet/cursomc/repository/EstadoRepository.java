package com.gmail.andersoninfonet.cursomc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gmail.andersoninfonet.cursomc.model.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long> {

}
