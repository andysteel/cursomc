package com.gmail.andersoninfonet.cursomc.service;

import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

public interface AuthService {

	default public void sendNewPassword(String email) throws ObjectNotFoundException {
		
	}
	
	default public String newPassword() {
		return null;
	}
	
	@SuppressWarnings("null")
	default public char randomChar() {
		return (Character) null;
	}
}
