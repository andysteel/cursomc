package com.gmail.andersoninfonet.cursomc.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.andersoninfonet.cursomc.model.Categoria;
import com.gmail.andersoninfonet.cursomc.repository.CategoriaRepository;
import com.gmail.andersoninfonet.cursomc.service.CategoriaService;
import com.gmail.andersoninfonet.cursomc.service.exception.DataIntegrityException;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

@Service
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Override
	public Categoria find(Long id) throws ObjectNotFoundException {
		Optional<Categoria> categoria = categoriaRepository.findById(id);
		return categoria.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! "
									+ "Id: " + id + ", Tipo: " + Categoria.class.getName()));
	}
	
	@Transactional
	@Override
	public Categoria insert(Categoria categoria) {
		return categoriaRepository.save(categoria);
	}
	
	@Transactional
	@Override
	public Categoria update(Categoria categoria) throws ObjectNotFoundException {
		Categoria categoriaResult = find(categoria.getId());
		updateData(categoriaResult, categoria);
		return categoriaRepository.save(categoriaResult);
	}
	
	private void updateData(Categoria categoriaResult, Categoria categoria) {
		categoriaResult.setNome(categoria.getNome());	
	}

	@Transactional
	@Override
	public void delete(Long id) throws ObjectNotFoundException, DataIntegrityException {
		find(id);
		try {
			categoriaRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir uma categoria que possui produtos.");
		}
	}
	
	@Override
	public List<Categoria> findAll() {
		return categoriaRepository.findAll();
	}
	
	@Override
	public Page<Categoria> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		
		return categoriaRepository.findAll(pageRequest);
	}
}
