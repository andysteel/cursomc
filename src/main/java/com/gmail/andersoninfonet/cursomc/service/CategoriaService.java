package com.gmail.andersoninfonet.cursomc.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.gmail.andersoninfonet.cursomc.dto.CategoriaDTO;
import com.gmail.andersoninfonet.cursomc.model.Categoria;
import com.gmail.andersoninfonet.cursomc.service.exception.DataIntegrityException;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

public interface CategoriaService {

	default public Categoria find(Long id) throws ObjectNotFoundException {
		return null;
	}

	default public Categoria insert(Categoria categoria) {
		return null;
	}

	default public Categoria update(Categoria categoria) throws ObjectNotFoundException {
		return null;
	}

	default public void delete(Long id) throws ObjectNotFoundException, DataIntegrityException {
		
	}

	default public List<Categoria> findAll() {
		return null;
	}
	
	default public Page<Categoria> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		return null;
	}
	
	default public Categoria fromDTO(CategoriaDTO dto) {
		return new Categoria(dto.getId(), dto.getNome());
	}
}
