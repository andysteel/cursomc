package com.gmail.andersoninfonet.cursomc.service.exception;

public class ObjectNotFoundException extends Exception {

	private static final long serialVersionUID = -6459672303283635446L;

	public ObjectNotFoundException(String message) {
		super(message);
	}

	public ObjectNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
