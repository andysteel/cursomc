package com.gmail.andersoninfonet.cursomc.service.exception;

public class DataIntegrityException extends Exception {

	private static final long serialVersionUID = -6459672303283635446L;

	public DataIntegrityException(String message) {
		super(message);
	}

	public DataIntegrityException(String message, Throwable cause) {
		super(message, cause);
	}

}
