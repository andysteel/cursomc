package com.gmail.andersoninfonet.cursomc.service;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;

import com.gmail.andersoninfonet.cursomc.model.Cliente;
import com.gmail.andersoninfonet.cursomc.model.Pedido;

public interface EmailService {

	default public void sendOrderConfirmationEmail(Pedido pedido) {
		
	}
	
	default public void sendMail(SimpleMailMessage msg) {
		
	}
	
	default public void sendOrderConfirmationHtmlEmail(Pedido obj) {
		
	}
	
	default public void sendHtmlEmail(MimeMessage msg) {
		
	}
	
	default public void sendNewPasswordEmail(Cliente cliente, String newPass) {
		
	}
}
