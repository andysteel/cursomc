package com.gmail.andersoninfonet.cursomc.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.andersoninfonet.cursomc.dto.ClienteNewDTO;
import com.gmail.andersoninfonet.cursomc.model.Cidade;
import com.gmail.andersoninfonet.cursomc.model.Cliente;
import com.gmail.andersoninfonet.cursomc.model.Endereco;
import com.gmail.andersoninfonet.cursomc.model.enums.Perfil;
import com.gmail.andersoninfonet.cursomc.model.enums.TipoCliente;
import com.gmail.andersoninfonet.cursomc.repository.CidadeRepository;
import com.gmail.andersoninfonet.cursomc.repository.ClienteRepository;
import com.gmail.andersoninfonet.cursomc.repository.EnderecoRepository;
import com.gmail.andersoninfonet.cursomc.security.Usuario;
import com.gmail.andersoninfonet.cursomc.service.ClienteService;
import com.gmail.andersoninfonet.cursomc.service.UserService;
import com.gmail.andersoninfonet.cursomc.service.exception.AuthorizationException;
import com.gmail.andersoninfonet.cursomc.service.exception.DataIntegrityException;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Override
	public Cliente find(Long id) throws ObjectNotFoundException {
		Usuario user = UserService.authenticated();
		if (user==null || !user.hasRole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
}
		
		Optional<Cliente> cliente = clienteRepository.findById(id);
		return cliente.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! "
									+ "Id: " + id + ", Tipo: " + Cliente.class.getName()));
	}
	
	@Transactional
	@Override
	public Cliente insert(Cliente cliente) {
		cliente = clienteRepository.save(cliente);
		enderecoRepository.saveAll(cliente.getEnderecos());
		return cliente;
	}
	
	@Transactional
	@Override
	public Cliente update(Cliente cliente) throws ObjectNotFoundException {
		Cliente resultCliente =  find(cliente.getId());
		updateData(resultCliente, cliente);
		return clienteRepository.save(resultCliente);
	}
	
	private void updateData(Cliente resultCliente, Cliente cliente) {
		resultCliente.setNome(cliente.getNome());
		resultCliente.setEmail(cliente.getEmail());
	}

	@Transactional
	@Override
	public void delete(Long id) throws ObjectNotFoundException, DataIntegrityException {
		find(id);
		try {
			clienteRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir um cliente porque há pedidos relacionados.");
		}
	}
	
	@Override
	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}
	
	@Override
	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		
		return clienteRepository.findAll(pageRequest);
	}
	
	@Override
	public Cliente fromDTO(ClienteNewDTO dto) {
		Cliente cliente = new Cliente(null, dto.getNome(), dto.getEmail(), dto.getCpfOuCnpj(), TipoCliente.toEnum(dto.getTipoCliente()), pe.encode(dto.getSenha()));
		Optional<Cidade> cidade = cidadeRepository.findById(dto.getCidadeId());
		if(cidade.isPresent()) {
			Endereco endereco = new Endereco(null, dto.getLogradouro(), dto.getNumero(), dto.getComplemento(), dto.getBairro(), dto.getCep(), cliente, cidade.get());
			cliente.getEnderecos().add(endereco);
		}
		cliente.getTelefones().add(dto.getTelefone1());
		if(dto.getTelefone2() != null) {
			cliente.getTelefones().add(dto.getTelefone2());
		}
		if(dto.getTelefone3() != null) {
			cliente.getTelefones().add(dto.getTelefone3());
		}
		
		return cliente;
	}
}
