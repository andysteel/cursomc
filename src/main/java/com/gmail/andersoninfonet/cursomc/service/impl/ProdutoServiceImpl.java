package com.gmail.andersoninfonet.cursomc.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.gmail.andersoninfonet.cursomc.model.Categoria;
import com.gmail.andersoninfonet.cursomc.model.Produto;
import com.gmail.andersoninfonet.cursomc.repository.CategoriaRepository;
import com.gmail.andersoninfonet.cursomc.repository.ProdutoRepository;
import com.gmail.andersoninfonet.cursomc.service.ProdutoService;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Override
	public Produto buscar(Long id) throws ObjectNotFoundException {
		Optional<Produto> produto = produtoRepository.findById(id);
		return produto.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! "
									+ "Id: " + id + ", Tipo: " + Produto.class.getName()));
	}
	
	@Override
	public Page<Produto> search(String nome, List<Long> ids, Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		List<Categoria> categorias = categoriaRepository.findAllById(ids);
		
		return produtoRepository.search(nome, categorias, pageRequest);
	}
}
