package com.gmail.andersoninfonet.cursomc.service.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import com.gmail.andersoninfonet.cursomc.controller.exception.FieldMessage;
import com.gmail.andersoninfonet.cursomc.dto.ClienteDTO;
import com.gmail.andersoninfonet.cursomc.model.Cliente;
import com.gmail.andersoninfonet.cursomc.repository.ClienteRepository;

public class ClienteUpdateValidator implements ConstraintValidator<ClienteUpdate, ClienteDTO>{

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public void initialize(ClienteUpdate constraintAnnotation) {
		
	}
	
	@Override
	public boolean isValid(ClienteDTO dto, ConstraintValidatorContext context) {
		
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		Long uriId = Long.parseLong(map.get("id"));
		
		List<FieldMessage> lista = new ArrayList<>();
		
		Cliente cliente = clienteRepository.findByEmail(dto.getEmail());
		if(cliente != null && !cliente.getId().equals(uriId)) {
			lista.add(new FieldMessage("email", "Email já existente."));
		}
		
		for(FieldMessage e : lista) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getField())
					.addConstraintViolation();
		}
		
		return lista.isEmpty();
	}

}
