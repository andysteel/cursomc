package com.gmail.andersoninfonet.cursomc.service.impl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.gmail.andersoninfonet.cursomc.model.PagamentoComBoleto;

@Service
public class BoletoService {

	public void preencherPagamentoComBoleto(PagamentoComBoleto pagto, Date dataDoPedido) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataDoPedido);
		calendar.add(Calendar.DAY_OF_MONTH, 7);
		pagto.setDataVencimento(calendar.getTime());
	}
}
