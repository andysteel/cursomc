package com.gmail.andersoninfonet.cursomc.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.gmail.andersoninfonet.cursomc.dto.ClienteDTO;
import com.gmail.andersoninfonet.cursomc.dto.ClienteNewDTO;
import com.gmail.andersoninfonet.cursomc.model.Cliente;
import com.gmail.andersoninfonet.cursomc.service.exception.DataIntegrityException;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

public interface ClienteService {

	default public Cliente find(Long id) throws ObjectNotFoundException {
		return null;
	}
	
	default public Cliente insert(Cliente cliente) {
		return null;
	}

	default public Cliente update(Cliente cliente) throws ObjectNotFoundException {
		return null;
	}

	default public void delete(Long id) throws ObjectNotFoundException, DataIntegrityException {
		
	}

	default public List<Cliente> findAll() {
		return null;
	}
	
	default public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		return null;
	}
	
	default public Cliente fromDTO(ClienteDTO dto) {
		return new Cliente(dto.getId(), dto.getNome(), dto.getEmail(), null, null, null);
	}
	
	default public Cliente fromDTO(ClienteNewDTO dto) {
		return null;
	}
}
