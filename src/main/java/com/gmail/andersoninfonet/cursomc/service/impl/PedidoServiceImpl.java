package com.gmail.andersoninfonet.cursomc.service.impl;

import static com.gmail.andersoninfonet.cursomc.model.enums.EstadoPagamento.PENDENTE;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.andersoninfonet.cursomc.model.Cliente;
import com.gmail.andersoninfonet.cursomc.model.ItemPedido;
import com.gmail.andersoninfonet.cursomc.model.PagamentoComBoleto;
import com.gmail.andersoninfonet.cursomc.model.Pedido;
import com.gmail.andersoninfonet.cursomc.repository.ItemPedidoRepository;
import com.gmail.andersoninfonet.cursomc.repository.PagamentoRepository;
import com.gmail.andersoninfonet.cursomc.repository.PedidoRepository;
import com.gmail.andersoninfonet.cursomc.security.Usuario;
import com.gmail.andersoninfonet.cursomc.service.ClienteService;
import com.gmail.andersoninfonet.cursomc.service.EmailService;
import com.gmail.andersoninfonet.cursomc.service.PedidoService;
import com.gmail.andersoninfonet.cursomc.service.ProdutoService;
import com.gmail.andersoninfonet.cursomc.service.UserService;
import com.gmail.andersoninfonet.cursomc.service.exception.AuthorizationException;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

@Service
public class PedidoServiceImpl implements PedidoService {

	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private BoletoService boletoService;
	
	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private EmailService emailService;
	
	@Override
	public Pedido buscar(Long id) throws ObjectNotFoundException {
		Optional<Pedido> pedido = pedidoRepository.findById(id);
		return pedido.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! "
									+ "Id: " + id + ", Tipo: " + Pedido.class.getName()));
	}
	
	@Transactional
	@Override
	public Pedido insert(Pedido pedido) throws ObjectNotFoundException {
		pedido.setId(null);
		pedido.setDataPedido(new Date());
		pedido.setCliente(clienteService.find(pedido.getCliente().getId()));
		pedido.getPagamento().setEstadoPagamento(PENDENTE);
		pedido.getPagamento().setPedido(pedido);
		
		if(pedido.getPagamento() instanceof PagamentoComBoleto) {
			PagamentoComBoleto pagto = (PagamentoComBoleto) pedido.getPagamento();
			boletoService.preencherPagamentoComBoleto(pagto, pedido.getDataPedido());
		}
		pedido = pedidoRepository.save(pedido);
		pagamentoRepository.save(pedido.getPagamento());
		
		for(ItemPedido item : pedido.getItens()) {
			item.setDesconto(0.0);
			try {
				item.setProduto(produtoService.buscar(item.getProduto().getId()));
			} catch (ObjectNotFoundException e) {
				throw new RuntimeException("Produto não encontrado.");
			}
				item.setPreco(item.getProduto().getPreco());
				item.setPedido(pedido);
		}
		itemPedidoRepository.saveAll(pedido.getItens());
		
		emailService.sendOrderConfirmationHtmlEmail(pedido);
		return pedido;
	}
	
	public Page<Pedido> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) throws ObjectNotFoundException {
		Usuario user = UserService.authenticated();
		if (user == null) {
			throw new AuthorizationException("Acesso negado");
		}
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		Cliente cliente =  clienteService.find(user.getId());
		return pedidoRepository.findByCliente(cliente, pageRequest);
	}
}
