package com.gmail.andersoninfonet.cursomc.service;

import org.springframework.data.domain.Page;

import com.gmail.andersoninfonet.cursomc.model.Pedido;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

public interface PedidoService {

	default public Pedido buscar(Long id) throws ObjectNotFoundException {
		return null;
	}
	
	default public Pedido insert(Pedido pedido) throws ObjectNotFoundException {
		return null;
	}
	
	default public Page<Pedido> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) throws ObjectNotFoundException {
		return null;
	}
}
