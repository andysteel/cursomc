package com.gmail.andersoninfonet.cursomc.service;

import org.springframework.security.core.context.SecurityContextHolder;

import com.gmail.andersoninfonet.cursomc.security.Usuario;

public class UserService {

	public static Usuario authenticated() {
		
		try {
			return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		catch (Exception e) {
			return null;
		}
	}
}
