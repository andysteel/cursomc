package com.gmail.andersoninfonet.cursomc.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.gmail.andersoninfonet.cursomc.model.Produto;
import com.gmail.andersoninfonet.cursomc.service.exception.ObjectNotFoundException;

public interface ProdutoService {

	default public Produto buscar(Long id) throws ObjectNotFoundException {
		return null;
	}
	
	default public Page<Produto> search(String nome, List<Long> ids, Integer page, Integer linesPerPage, String orderBy, String direction) {
		return null;
	}
}
