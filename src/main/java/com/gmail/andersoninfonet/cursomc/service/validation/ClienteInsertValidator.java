package com.gmail.andersoninfonet.cursomc.service.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.gmail.andersoninfonet.cursomc.controller.exception.FieldMessage;
import com.gmail.andersoninfonet.cursomc.dto.ClienteNewDTO;
import com.gmail.andersoninfonet.cursomc.model.Cliente;
import com.gmail.andersoninfonet.cursomc.model.enums.TipoCliente;
import com.gmail.andersoninfonet.cursomc.repository.ClienteRepository;
import com.gmail.andersoninfonet.cursomc.service.validation.utils.BR;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, ClienteNewDTO>{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public void initialize(ClienteInsert constraintAnnotation) {
		
	}
	
	@Override
	public boolean isValid(ClienteNewDTO dto, ConstraintValidatorContext context) {
		List<FieldMessage> lista = new ArrayList<>();
		
		if(dto.getTipoCliente().equals(TipoCliente.PESSOA_FISICA.getCod()) && !BR.isValidCPF(dto.getCpfOuCnpj())) {
			lista.add(new FieldMessage("cpfOuCnpj", "CPF Inválido"));
		}
		
		if(dto.getTipoCliente().equals(TipoCliente.PESSOA_JURIDICA.getCod()) && !BR.isValidCNPJ(dto.getCpfOuCnpj())) {
			lista.add(new FieldMessage("cpfOuCnpj", "CNPJ Inválido"));
		}
		
		Cliente cliente = clienteRepository.findByEmail(dto.getEmail());
		if(cliente != null) {
			lista.add(new FieldMessage("email", "Email já existente."));
		}
		
		for(FieldMessage e : lista) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getField())
					.addConstraintViolation();
		}
		
		return lista.isEmpty();
	}

}
