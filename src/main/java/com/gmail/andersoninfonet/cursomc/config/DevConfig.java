package com.gmail.andersoninfonet.cursomc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.gmail.andersoninfonet.cursomc.service.DBService;
import com.gmail.andersoninfonet.cursomc.service.EmailService;
import com.gmail.andersoninfonet.cursomc.service.MockEmailService;

@Configuration
@Profile("development")
public class DevConfig {
	
	@Autowired
	private DBService dbService;

	@Bean
	public boolean instantiateDataBase() throws Exception {
		dbService.instantiateDevDataBase();
		return true;
	}
	
	@Bean
	public EmailService emailService() {
		return new MockEmailService();
	}
	
	@Bean
	public JavaMailSender javaMailSender() {
		return new JavaMailSenderImpl();
	}
	
}
