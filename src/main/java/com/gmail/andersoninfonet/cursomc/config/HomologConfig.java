package com.gmail.andersoninfonet.cursomc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.gmail.andersoninfonet.cursomc.service.DBService;
import com.gmail.andersoninfonet.cursomc.service.EmailService;
import com.gmail.andersoninfonet.cursomc.service.impl.SmtpEmailService;

@Configuration
@Profile("homolog")
public class HomologConfig {
	
	@Autowired
	private DBService dbService;
	
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String strategy;

	@Bean
	public boolean instantiateDataBase() throws Exception {
		
		if(!"create".equals(strategy)) {
			return false;
		}
		dbService.instantiateDevDataBase();
		return true;
	}
	
	@Bean
	public EmailService emailService() {
		return new SmtpEmailService();
	}
	
}
