package com.gmail.andersoninfonet.cursomc.model.enums;

public enum Perfil {

	ADMIN(1,"ROLE_ADMIN"),
	CLIENTE(2,"ROLE_CLIENTE");
	
	private long codigo;
	private String nome;
	
	private Perfil(long codigo,String nome) {
		this.codigo = codigo;
		this.nome = nome;
	}
	
	public long getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public static Perfil toEnum(Long cod) {
		
		if(cod == null) {
			return null;
		}
		
		for(Perfil x : Perfil.values()) {
			
			if(cod.equals(x.getCodigo())) {
				return x;
			}
		}
		throw new IllegalArgumentException("Id inválido " + cod);
	}
}
